Examen Interciclo Computacion Paralela

A continuacion se explicara la resolucion del mismo.

Dada la siguiente funcion:

![1](/uploads/8987b112fbe1c4a1e1ad730f5d06b8ac/1.jpg)

Se pide mejorar el tiempo en 2x utilizando algoritmos en paralelo, tales como: 

1. multiprocessing.

Para ello se creo la siguiente funcion que utiliza multiprocessing.

![2](/uploads/8c0f0393021acc56bb77af592a7a9e6d/2.jpg)

Esta funcion recibe la lista y la divide en 8 porciones iguales de 500000, los mismos que seran pasados como parametros dentro de la funcion **how_many_max_values_sequential** 
que estara dentro de cada proceso. El resultado de la funcion **how_many_max_values_sequential**  se ira guardando en una lista

2. MPI

Para ello se creo la siguiente funcion que utiliza MPI.

![3](/uploads/8ae72fff78964d9817569f431e9d5e35/3.jpg)

En donde data1 y data2 son los rangos de numeros a pasar de la lista y tam es el tamano de dichos rangos, numWorkers es el numero de procesos a correr.

Luego se crea dos bucles for que recorreran desde 0 hasta el numero de procesos que se correran, en el primer for enviamos data1 y data2, es decir el rango de numeros
posteriormente aumentamos el valor de data1 asignandole el valor de data2 y a data2 aumentando tam. En el siguiente for se recibe lo enviado y luego se llama a la funcion
**how_many_max_values_sequential**  pasando como argumento el rango de numeros.

3. Visualizamos los resultados

![4](/uploads/783850b8dc28763ce515f394c70107e7/4.jpg)

Inicializamos tiempos, llamamos a las funciones y finalizamos tiempos. Obteniendo los siguientes resultados:


**Sequential Process took 5045.758 ms with 40000000 items**

**Parallel Process took 1467.906 ms with 40000000 items**

