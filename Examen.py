import random

import time
import multiprocessing
from mpi4py import MPI 

 

 

def how_many_max_values_sequential(ar):

    #find max value of the list

    maxValue = 0

    for i in range(len(ar)):

        if i == 0:

            maxValue = ar[i]

        else:

            if ar[i] > maxValue:

                maxValue = ar[i]

    #find how many max values are in the list

    contValue = 0

    for i in range(len(ar)):

        if ar[i] == maxValue:

            contValue += 1


    return contValue

 

# Complete the how_many_max_values_parallel function below.

def how_many_max_values_parallelProceso(ar):
   resultado=list()
   
   proceso1 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[0:5000000],resultado))
   proceso2 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[5000001:10000000],resultado))
   proceso3 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[10000001:15000000],resultado))
   proceso4 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[15000001:20000000],resultado))
   proceso5 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[20000001:25000000],resultado))
   proceso6 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[25000001:30000000],resultado))
   proceso7 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[30000001:35000000],resultado))
   proceso8 = multiprocessing.Process(target=how_many_max_values_sequential,args=(ar[35000001:40000001],resultado))
                                                                                             
   proceso1.start()  
   proceso2.start() 
   proceso3.start() 
   proceso4.start() 
   proceso5.start() 
   proceso6.start()
   proceso7.start() 
   proceso8.start() 


   proceso1.join() 
   proceso2.join() 
   proceso3.join() 
   proceso4.join() 
   proceso5.join() 
   proceso6.join()
   proceso7.join() 
   proceso8.join()
   
   return resultado

def how_many_max_values_parallelMpi(ar):
   
   resultado=0
   data1= 0
   tam= 5000000
   data2= 5000000
   
   comm = MPI.COMM_WORLD 
   numtasks = comm.size 
   rank = comm.Get_rank()

   numworkers = numtasks-1
   
   
   for destino in range(numworkers):

       comm.send(data1,dest=destino+1)
       comm.send(data2,dest=destino+1)
       data1=data2+1
       data2=data2+tam
    
   for fuente in range(numworkers):
       
       data1=comm.recv(source=fuente+1)
       data2=comm.recv(source=fuente+1)
    
       resultado=how_many_max_values_sequential(ar[data1:data2])
   
   return resultado
    
    #implement your solution

 

if __name__ == '__main__':
    resultado=list()

    ar_count = 40000000

    #Generate ar_count random numbers between 1 and 30

    ar = [random.randrange(1,30) for i in range(ar_count)]
    
    

    inicioSec = time.time()

    resultSec = how_many_max_values_sequential(ar)

    finSec =  time.time()

   

    inicioPar = time.time()   

    resultPar = how_many_max_values_parallelMpi(ar)
   

    finPar = time.time()   

   

    print('Results are correct!\n' if resultSec == resultPar else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms with %d items\n' % ((finSec - inicioSec)*1000, ar_count))

    print('Parallel Process took %.3f ms with %d items\n' % ((finPar - inicioPar)*1000, ar_count))





